# google-cloud-roles

This module lets you avoid wasting time searching for roles and their correct spelling.

```tf
// setup module
module "roles" {
  source = "git::https://gitlab.com/ekinox-io/ekinox-libraries/google-cloud-roles.git?ref=v1.0.0"
}

locals {
  bq_admin = module.roles.bigquery.jobUser
}
```

## License
MIT License

Copyright (c) 2022, Ekinox
