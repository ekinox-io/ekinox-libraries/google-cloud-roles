output "accessapproval" {
  description = "Roles of service accessapproval"
  value       = {
    "approver" : "roles/accessapproval.approver",
    "configEditor" : "roles/accessapproval.configEditor",
    "invalidator" : "roles/accessapproval.invalidator",
    "viewer" : "roles/accessapproval.viewer"
  }
}

output "accesscontextmanager" {
  description = "Roles of service accesscontextmanager"
  value       = {
    "gcpAccessAdmin" : "roles/accesscontextmanager.gcpAccessAdmin",
    "gcpAccessReader" : "roles/accesscontextmanager.gcpAccessReader",
    "policyAdmin" : "roles/accesscontextmanager.policyAdmin",
    "policyEditor" : "roles/accesscontextmanager.policyEditor",
    "policyReader" : "roles/accesscontextmanager.policyReader",
    "vpcScTroubleshooterViewer" : "roles/accesscontextmanager.vpcScTroubleshooterViewer"
  }
}

output "actions" {
  description = "Roles of service actions"
  value       = {
    "Admin" : "roles/actions.Admin",
    "Viewer" : "roles/actions.Viewer"
  }
}

output "notebooks" {
  description = "Roles of service notebooks"
  value       = {
    "admin" : "roles/notebooks.admin",
    "legacyAdmin" : "roles/notebooks.legacyAdmin",
    "legacyViewer" : "roles/notebooks.legacyViewer",
    "runner" : "roles/notebooks.runner",
    "serviceAgent" : "roles/notebooks.serviceAgent",
    "viewer" : "roles/notebooks.viewer"
  }
}

output "ml" {
  description = "Roles of service ml"
  value       = {
    "admin" : "roles/ml.admin",
    "developer" : "roles/ml.developer",
    "jobOwner" : "roles/ml.jobOwner",
    "modelOwner" : "roles/ml.modelOwner",
    "modelUser" : "roles/ml.modelUser",
    "operationOwner" : "roles/ml.operationOwner",
    "serviceAgent" : "roles/ml.serviceAgent",
    "viewer" : "roles/ml.viewer"
  }
}

output "analyticshub" {
  description = "Roles of service analyticshub"
  value       = {
    "admin" : "roles/analyticshub.admin",
    "listingAdmin" : "roles/analyticshub.listingAdmin",
    "publisher" : "roles/analyticshub.publisher",
    "subscriber" : "roles/analyticshub.subscriber",
    "viewer" : "roles/analyticshub.viewer"
  }
}

output "androidmanagement" {
  description = "Roles of service androidmanagement"
  value       = {
    "user" : "roles/androidmanagement.user"
  }
}

output "gkemulticloud" {
  description = "Roles of service gkemulticloud"
  value       = {
    "admin" : "roles/gkemulticloud.admin",
    "serviceAgent" : "roles/gkemulticloud.serviceAgent",
    "telemetryWriter" : "roles/gkemulticloud.telemetryWriter",
    "viewer" : "roles/gkemulticloud.viewer"
  }
}

output "apigateway" {
  description = "Roles of service apigateway"
  value       = {
    "admin" : "roles/apigateway.admin",
    "serviceAgent" : "roles/apigateway.serviceAgent",
    "viewer" : "roles/apigateway.viewer"
  }
}

output "apigee" {
  description = "Roles of service apigee"
  value       = {
    "admin" : "roles/apigee.admin",
    "analyticsAgent" : "roles/apigee.analyticsAgent",
    "analyticsEditor" : "roles/apigee.analyticsEditor",
    "analyticsViewer" : "roles/apigee.analyticsViewer",
    "apiAdminV2" : "roles/apigee.apiAdminV2",
    "apiReaderV2" : "roles/apigee.apiReaderV2",
    "developerAdmin" : "roles/apigee.developerAdmin",
    "environmentAdmin" : "roles/apigee.environmentAdmin",
    "monetizationAdmin" : "roles/apigee.monetizationAdmin",
    "portalAdmin" : "roles/apigee.portalAdmin",
    "readOnlyAdmin" : "roles/apigee.readOnlyAdmin",
    "runtimeAgent" : "roles/apigee.runtimeAgent",
    "securityAdmin" : "roles/apigee.securityAdmin",
    "securityViewer" : "roles/apigee.securityViewer",
    "serviceAgent" : "roles/apigee.serviceAgent",
    "synchronizerManager" : "roles/apigee.synchronizerManager"
  }
}

output "apigeeconnect" {
  description = "Roles of service apigeeconnect"
  value       = {
    "Admin" : "roles/apigeeconnect.Admin",
    "Agent" : "roles/apigeeconnect.Agent"
  }
}

output "apigeeregistry" {
  description = "Roles of service apigeeregistry"
  value       = {
    "admin" : "roles/apigeeregistry.admin",
    "editor" : "roles/apigeeregistry.editor",
    "viewer" : "roles/apigeeregistry.viewer",
    "worker" : "roles/apigeeregistry.worker"
  }
}

output "appengine" {
  description = "Roles of service appengine"
  value       = {
    "appAdmin" : "roles/appengine.appAdmin",
    "appCreator" : "roles/appengine.appCreator",
    "appViewer" : "roles/appengine.appViewer",
    "codeViewer" : "roles/appengine.codeViewer",
    "deployer" : "roles/appengine.deployer",
    "memcacheDataAdmin" : "roles/appengine.memcacheDataAdmin",
    "serviceAdmin" : "roles/appengine.serviceAdmin",
    "serviceAgent" : "roles/appengine.serviceAgent"
  }
}

output "artifactregistry" {
  description = "Roles of service artifactregistry"
  value       = {
    "admin" : "roles/artifactregistry.admin",
    "reader" : "roles/artifactregistry.reader",
    "repoAdmin" : "roles/artifactregistry.repoAdmin",
    "serviceAgent" : "roles/artifactregistry.serviceAgent",
    "writer" : "roles/artifactregistry.writer"
  }
}

output "assuredworkloads" {
  description = "Roles of service assuredworkloads"
  value       = {
    "admin" : "roles/assuredworkloads.admin",
    "editor" : "roles/assuredworkloads.editor",
    "reader" : "roles/assuredworkloads.reader",
    "serviceAgent" : "roles/assuredworkloads.serviceAgent"
  }
}

output "automl" {
  description = "Roles of service automl"
  value       = {
    "admin" : "roles/automl.admin",
    "editor" : "roles/automl.editor",
    "predictor" : "roles/automl.predictor",
    "serviceAgent" : "roles/automl.serviceAgent",
    "viewer" : "roles/automl.viewer"
  }
}

output "backupdr" {
  description = "Roles of service backupdr"
  value       = {
    "admin" : "roles/backupdr.admin",
    "cloudStorageOperator" : "roles/backupdr.cloudStorageOperator",
    "computeEngineOperator" : "roles/backupdr.computeEngineOperator",
    "user" : "roles/backupdr.user",
    "viewer" : "roles/backupdr.viewer"
  }
}

output "gkebackup" {
  description = "Roles of service gkebackup"
  value       = {
    "admin" : "roles/gkebackup.admin",
    "backupAdmin" : "roles/gkebackup.backupAdmin",
    "delegatedBackupAdmin" : "roles/gkebackup.delegatedBackupAdmin",
    "delegatedRestoreAdmin" : "roles/gkebackup.delegatedRestoreAdmin",
    "restoreAdmin" : "roles/gkebackup.restoreAdmin",
    "serviceAgent" : "roles/gkebackup.serviceAgent",
    "viewer" : "roles/gkebackup.viewer"
  }
}

output "beyondcorp" {
  description = "Roles of service beyondcorp"
  value       = {
    "admin" : "roles/beyondcorp.admin",
    "clientConnectorAdmin" : "roles/beyondcorp.clientConnectorAdmin",
    "clientConnectorServiceUser" : "roles/beyondcorp.clientConnectorServiceUser",
    "clientConnectorViewer" : "roles/beyondcorp.clientConnectorViewer",
    "viewer" : "roles/beyondcorp.viewer"
  }
}

output "bigquery" {
  description = "Roles of service bigquery"
  value       = {
    "admin" : "roles/bigquery.admin",
    "connectionAdmin" : "roles/bigquery.connectionAdmin",
    "connectionUser" : "roles/bigquery.connectionUser",
    "dataEditor" : "roles/bigquery.dataEditor",
    "dataOwner" : "roles/bigquery.dataOwner",
    "dataViewer" : "roles/bigquery.dataViewer",
    "filteredDataViewer" : "roles/bigquery.filteredDataViewer",
    "jobUser" : "roles/bigquery.jobUser",
    "metadataViewer" : "roles/bigquery.metadataViewer",
    "readSessionUser" : "roles/bigquery.readSessionUser",
    "resourceAdmin" : "roles/bigquery.resourceAdmin",
    "resourceEditor" : "roles/bigquery.resourceEditor",
    "resourceViewer" : "roles/bigquery.resourceViewer",
    "user" : "roles/bigquery.user"
  }
}

output "bigquerydatapolicy" {
  description = "Roles of service bigquerydatapolicy"
  value       = {
    "maskedReader" : "roles/bigquerydatapolicy.maskedReader"
  }
}

output "billing" {
  description = "Roles of service billing"
  value       = {
    "admin" : "roles/billing.admin",
    "carbonViewer" : "roles/billing.carbonViewer",
    "costsManager" : "roles/billing.costsManager",
    "creator" : "roles/billing.creator",
    "projectManager" : "roles/billing.projectManager",
    "user" : "roles/billing.user",
    "viewer" : "roles/billing.viewer"
  }
}

output "binaryauthorization" {
  description = "Roles of service binaryauthorization"
  value       = {
    "attestorsAdmin" : "roles/binaryauthorization.attestorsAdmin",
    "attestorsEditor" : "roles/binaryauthorization.attestorsEditor",
    "attestorsVerifier" : "roles/binaryauthorization.attestorsVerifier",
    "attestorsViewer" : "roles/binaryauthorization.attestorsViewer",
    "policyAdmin" : "roles/binaryauthorization.policyAdmin",
    "policyEditor" : "roles/binaryauthorization.policyEditor",
    "policyEvaluator" : "roles/binaryauthorization.policyEvaluator",
    "policyViewer" : "roles/binaryauthorization.policyViewer",
    "serviceAgent" : "roles/binaryauthorization.serviceAgent"
  }
}

output "privateca" {
  description = "Roles of service privateca"
  value       = {
    "admin" : "roles/privateca.admin",
    "auditor" : "roles/privateca.auditor",
    "caManager" : "roles/privateca.caManager",
    "certificateManager" : "roles/privateca.certificateManager",
    "certificateRequester" : "roles/privateca.certificateRequester",
    "templateUser" : "roles/privateca.templateUser",
    "workloadCertificateRequester" : "roles/privateca.workloadCertificateRequester"
  }
}

output "certificatemanager" {
  description = "Roles of service certificatemanager"
  value       = {
    "editor" : "roles/certificatemanager.editor",
    "owner" : "roles/certificatemanager.owner",
    "viewer" : "roles/certificatemanager.viewer"
  }
}

output "alloydb" {
  description = "Roles of service alloydb"
  value       = {
    "admin" : "roles/alloydb.admin",
    "client" : "roles/alloydb.client",
    "serviceAgent" : "roles/alloydb.serviceAgent",
    "viewer" : "roles/alloydb.viewer"
  }
}

output "cloudasset" {
  description = "Roles of service cloudasset"
  value       = {
    "owner" : "roles/cloudasset.owner",
    "serviceAgent" : "roles/cloudasset.serviceAgent",
    "viewer" : "roles/cloudasset.viewer"
  }
}

output "bigtable" {
  description = "Roles of service bigtable"
  value       = {
    "admin" : "roles/bigtable.admin",
    "reader" : "roles/bigtable.reader",
    "user" : "roles/bigtable.user",
    "viewer" : "roles/bigtable.viewer"
  }
}

output "cloudbuild" {
  description = "Roles of service cloudbuild"
  value       = {
    "builds_approver" : "roles/cloudbuild.builds.approver",
    "builds_builder" : "roles/cloudbuild.builds.builder",
    "builds_editor" : "roles/cloudbuild.builds.editor",
    "builds_viewer" : "roles/cloudbuild.builds.viewer",
    "integrationsEditor" : "roles/cloudbuild.integrationsEditor",
    "integrationsOwner" : "roles/cloudbuild.integrationsOwner",
    "integrationsViewer" : "roles/cloudbuild.integrationsViewer",
    "serviceAgent" : "roles/cloudbuild.serviceAgent",
    "workerPoolEditor" : "roles/cloudbuild.workerPoolEditor",
    "workerPoolOwner" : "roles/cloudbuild.workerPoolOwner",
    "workerPoolUser" : "roles/cloudbuild.workerPoolUser",
    "workerPoolViewer" : "roles/cloudbuild.workerPoolViewer"
  }
}

output "composer" {
  description = "Roles of service composer"
  value       = {
    "ServiceAgentV2Ext" : "roles/composer.ServiceAgentV2Ext",
    "admin" : "roles/composer.admin",
    "environmentAndStorageObjectAdmin" : "roles/composer.environmentAndStorageObjectAdmin",
    "environmentAndStorageObjectViewer" : "roles/composer.environmentAndStorageObjectViewer",
    "serviceAgent" : "roles/composer.serviceAgent",
    "sharedVpcAgent" : "roles/composer.sharedVpcAgent",
    "user" : "roles/composer.user",
    "worker" : "roles/composer.worker"
  }
}

output "connectors" {
  description = "Roles of service connectors"
  value       = {
    "admin" : "roles/connectors.admin",
    "invoker" : "roles/connectors.invoker",
    "viewer" : "roles/connectors.viewer"
  }
}

output "datafusion" {
  description = "Roles of service datafusion"
  value       = {
    "admin" : "roles/datafusion.admin",
    "runner" : "roles/datafusion.runner",
    "serviceAgent" : "roles/datafusion.serviceAgent",
    "viewer" : "roles/datafusion.viewer"
  }
}

output "datalabeling" {
  description = "Roles of service datalabeling"
  value       = {
    "admin" : "roles/datalabeling.admin",
    "editor" : "roles/datalabeling.editor",
    "serviceAgent" : "roles/datalabeling.serviceAgent",
    "viewer" : "roles/datalabeling.viewer"
  }
}

output "dataplex" {
  description = "Roles of service dataplex"
  value       = {
    "admin" : "roles/dataplex.admin",
    "dataOwner" : "roles/dataplex.dataOwner",
    "dataReader" : "roles/dataplex.dataReader",
    "dataWriter" : "roles/dataplex.dataWriter",
    "developer" : "roles/dataplex.developer",
    "editor" : "roles/dataplex.editor",
    "metadataReader" : "roles/dataplex.metadataReader",
    "metadataWriter" : "roles/dataplex.metadataWriter",
    "serviceAgent" : "roles/dataplex.serviceAgent",
    "storageDataOwner" : "roles/dataplex.storageDataOwner",
    "storageDataReader" : "roles/dataplex.storageDataReader",
    "storageDataWriter" : "roles/dataplex.storageDataWriter",
    "viewer" : "roles/dataplex.viewer"
  }
}

output "clouddebugger" {
  description = "Roles of service clouddebugger"
  value       = {
    "agent" : "roles/clouddebugger.agent",
    "user" : "roles/clouddebugger.user"
  }
}

output "clouddeploy" {
  description = "Roles of service clouddeploy"
  value       = {
    "admin" : "roles/clouddeploy.admin",
    "approver" : "roles/clouddeploy.approver",
    "developer" : "roles/clouddeploy.developer",
    "jobRunner" : "roles/clouddeploy.jobRunner",
    "operator" : "roles/clouddeploy.operator",
    "releaser" : "roles/clouddeploy.releaser",
    "serviceAgent" : "roles/clouddeploy.serviceAgent",
    "viewer" : "roles/clouddeploy.viewer"
  }
}

output "dlp" {
  description = "Roles of service dlp"
  value       = {
    "admin" : "roles/dlp.admin",
    "analyzeRiskTemplatesEditor" : "roles/dlp.analyzeRiskTemplatesEditor",
    "analyzeRiskTemplatesReader" : "roles/dlp.analyzeRiskTemplatesReader",
    "columnDataProfilesReader" : "roles/dlp.columnDataProfilesReader",
    "dataProfilesReader" : "roles/dlp.dataProfilesReader",
    "deidentifyTemplatesEditor" : "roles/dlp.deidentifyTemplatesEditor",
    "deidentifyTemplatesReader" : "roles/dlp.deidentifyTemplatesReader",
    "estimatesAdmin" : "roles/dlp.estimatesAdmin",
    "inspectFindingsReader" : "roles/dlp.inspectFindingsReader",
    "inspectTemplatesEditor" : "roles/dlp.inspectTemplatesEditor",
    "inspectTemplatesReader" : "roles/dlp.inspectTemplatesReader",
    "jobTriggersEditor" : "roles/dlp.jobTriggersEditor",
    "jobTriggersReader" : "roles/dlp.jobTriggersReader",
    "jobsEditor" : "roles/dlp.jobsEditor",
    "jobsReader" : "roles/dlp.jobsReader",
    "orgdriver" : "roles/dlp.orgdriver",
    "projectDataProfilesReader" : "roles/dlp.projectDataProfilesReader",
    "projectdriver" : "roles/dlp.projectdriver",
    "reader" : "roles/dlp.reader",
    "serviceAgent" : "roles/dlp.serviceAgent",
    "storedInfoTypesEditor" : "roles/dlp.storedInfoTypesEditor",
    "storedInfoTypesReader" : "roles/dlp.storedInfoTypesReader",
    "tableDataProfilesReader" : "roles/dlp.tableDataProfilesReader",
    "user" : "roles/dlp.user"
  }
}

output "domains" {
  description = "Roles of service domains"
  value       = {
    "admin" : "roles/domains.admin",
    "viewer" : "roles/domains.viewer"
  }
}

output "file" {
  description = "Roles of service file"
  value       = {
    "editor" : "roles/file.editor",
    "serviceAgent" : "roles/file.serviceAgent",
    "viewer" : "roles/file.viewer"
  }
}

output "cloudfunctions" {
  description = "Roles of service cloudfunctions"
  value       = {
    "admin" : "roles/cloudfunctions.admin",
    "developer" : "roles/cloudfunctions.developer",
    "invoker" : "roles/cloudfunctions.invoker",
    "serviceAgent" : "roles/cloudfunctions.serviceAgent",
    "viewer" : "roles/cloudfunctions.viewer"
  }
}

output "gameservices" {
  description = "Roles of service gameservices"
  value       = {
    "admin" : "roles/gameservices.admin",
    "serviceAgent" : "roles/gameservices.serviceAgent",
    "viewer" : "roles/gameservices.viewer"
  }
}

output "healthcare" {
  description = "Roles of service healthcare"
  value       = {
    "annotationEditor" : "roles/healthcare.annotationEditor",
    "annotationReader" : "roles/healthcare.annotationReader",
    "annotationStoreAdmin" : "roles/healthcare.annotationStoreAdmin",
    "annotationStoreViewer" : "roles/healthcare.annotationStoreViewer",
    "attributeDefinitionEditor" : "roles/healthcare.attributeDefinitionEditor",
    "attributeDefinitionReader" : "roles/healthcare.attributeDefinitionReader",
    "consentArtifactAdmin" : "roles/healthcare.consentArtifactAdmin",
    "consentArtifactEditor" : "roles/healthcare.consentArtifactEditor",
    "consentArtifactReader" : "roles/healthcare.consentArtifactReader",
    "consentEditor" : "roles/healthcare.consentEditor",
    "consentReader" : "roles/healthcare.consentReader",
    "consentStoreAdmin" : "roles/healthcare.consentStoreAdmin",
    "consentStoreViewer" : "roles/healthcare.consentStoreViewer",
    "datasetAdmin" : "roles/healthcare.datasetAdmin",
    "datasetViewer" : "roles/healthcare.datasetViewer",
    "dicomEditor" : "roles/healthcare.dicomEditor",
    "dicomStoreAdmin" : "roles/healthcare.dicomStoreAdmin",
    "dicomStoreViewer" : "roles/healthcare.dicomStoreViewer",
    "dicomViewer" : "roles/healthcare.dicomViewer",
    "fhirResourceEditor" : "roles/healthcare.fhirResourceEditor",
    "fhirResourceReader" : "roles/healthcare.fhirResourceReader",
    "fhirStoreAdmin" : "roles/healthcare.fhirStoreAdmin",
    "fhirStoreViewer" : "roles/healthcare.fhirStoreViewer",
    "hl7V2Consumer" : "roles/healthcare.hl7V2Consumer",
    "hl7V2Editor" : "roles/healthcare.hl7V2Editor",
    "hl7V2Ingest" : "roles/healthcare.hl7V2Ingest",
    "hl7V2StoreAdmin" : "roles/healthcare.hl7V2StoreAdmin",
    "hl7V2StoreViewer" : "roles/healthcare.hl7V2StoreViewer",
    "nlpServiceViewer" : "roles/healthcare.nlpServiceViewer",
    "serviceAgent" : "roles/healthcare.serviceAgent",
    "userDataMappingEditor" : "roles/healthcare.userDataMappingEditor",
    "userDataMappingReader" : "roles/healthcare.userDataMappingReader"
  }
}

output "iap" {
  description = "Roles of service iap"
  value       = {
    "admin" : "roles/iap.admin",
    "httpsResourceAccessor" : "roles/iap.httpsResourceAccessor",
    "settingsAdmin" : "roles/iap.settingsAdmin",
    "tunnelDestGroupEditor" : "roles/iap.tunnelDestGroupEditor",
    "tunnelDestGroupViewer" : "roles/iap.tunnelDestGroupViewer",
    "tunnelResourceAccessor" : "roles/iap.tunnelResourceAccessor"
  }
}

output "ids" {
  description = "Roles of service ids"
  value       = {
    "admin" : "roles/ids.admin",
    "viewer" : "roles/ids.viewer"
  }
}

output "cloudiot" {
  description = "Roles of service cloudiot"
  value       = {
    "admin" : "roles/cloudiot.admin",
    "deviceController" : "roles/cloudiot.deviceController",
    "editor" : "roles/cloudiot.editor",
    "provisioner" : "roles/cloudiot.provisioner",
    "serviceAgent" : "roles/cloudiot.serviceAgent",
    "viewer" : "roles/cloudiot.viewer"
  }
}

output "cloudkms" {
  description = "Roles of service cloudkms"
  value       = {
    "admin" : "roles/cloudkms.admin",
    "cryptoKeyDecrypter" : "roles/cloudkms.cryptoKeyDecrypter",
    "cryptoKeyDecrypterViaDelegation" : "roles/cloudkms.cryptoKeyDecrypterViaDelegation",
    "cryptoKeyEncrypter" : "roles/cloudkms.cryptoKeyEncrypter",
    "cryptoKeyEncrypterDecrypter" : "roles/cloudkms.cryptoKeyEncrypterDecrypter",
    "cryptoKeyEncrypterDecrypterViaDelegation" : "roles/cloudkms.cryptoKeyEncrypterDecrypterViaDelegation",
    "cryptoKeyEncrypterViaDelegation" : "roles/cloudkms.cryptoKeyEncrypterViaDelegation",
    "cryptoOperator" : "roles/cloudkms.cryptoOperator",
    "expertRawPKCS1" : "roles/cloudkms.expertRawPKCS1",
    "importer" : "roles/cloudkms.importer",
    "publicKeyViewer" : "roles/cloudkms.publicKeyViewer",
    "serviceAgent" : "roles/cloudkms.serviceAgent",
    "signer" : "roles/cloudkms.signer",
    "signerVerifier" : "roles/cloudkms.signerVerifier",
    "verifier" : "roles/cloudkms.verifier",
    "viewer" : "roles/cloudkms.viewer"
  }
}

output "lifesciences" {
  description = "Roles of service lifesciences"
  value       = {
    "admin" : "roles/lifesciences.admin",
    "editor" : "roles/lifesciences.editor",
    "serviceAgent" : "roles/lifesciences.serviceAgent",
    "viewer" : "roles/lifesciences.viewer",
    "workflowsRunner" : "roles/lifesciences.workflowsRunner"
  }
}

output "managedidentities" {
  description = "Roles of service managedidentities"
  value       = {
    "admin" : "roles/managedidentities.admin",
    "backupAdmin" : "roles/managedidentities.backupAdmin",
    "backupViewer" : "roles/managedidentities.backupViewer",
    "domainAdmin" : "roles/managedidentities.domainAdmin",
    "peeringAdmin" : "roles/managedidentities.peeringAdmin",
    "peeringViewer" : "roles/managedidentities.peeringViewer",
    "serviceAgent" : "roles/managedidentities.serviceAgent",
    "viewer" : "roles/managedidentities.viewer"
  }
}

output "commerceoffercatalog" {
  description = "Roles of service commerceoffercatalog"
  value       = {
    "offersViewer" : "roles/commerceoffercatalog.offersViewer"
  }
}

output "commercepricemanagement" {
  description = "Roles of service commercepricemanagement"
  value       = {
    "privateOffersAdmin" : "roles/commercepricemanagement.privateOffersAdmin",
    "viewer" : "roles/commercepricemanagement.viewer"
  }
}

output "consumerprocurement" {
  description = "Roles of service consumerprocurement"
  value       = {
    "entitlementManager" : "roles/consumerprocurement.entitlementManager",
    "entitlementViewer" : "roles/consumerprocurement.entitlementViewer",
    "orderAdmin" : "roles/consumerprocurement.orderAdmin",
    "orderViewer" : "roles/consumerprocurement.orderViewer"
  }
}

output "cloudmigration" {
  description = "Roles of service cloudmigration"
  value       = {
    "inframanager" : "roles/cloudmigration.inframanager",
    "storageaccess" : "roles/cloudmigration.storageaccess",
    "velostrataconnect" : "roles/cloudmigration.velostrataconnect"
  }
}

output "vmmigration" {
  description = "Roles of service vmmigration"
  value       = {
    "admin" : "roles/vmmigration.admin",
    "viewer" : "roles/vmmigration.viewer"
  }
}

output "cloudprivatecatalog" {
  description = "Roles of service cloudprivatecatalog"
  value       = {
    "consumer" : "roles/cloudprivatecatalog.consumer"
  }
}

output "cloudprivatecatalogproducer" {
  description = "Roles of service cloudprivatecatalogproducer"
  value       = {
    "admin" : "roles/cloudprivatecatalogproducer.admin",
    "manager" : "roles/cloudprivatecatalogproducer.manager",
    "orgAdmin" : "roles/cloudprivatecatalogproducer.orgAdmin"
  }
}

output "cloudprofiler" {
  description = "Roles of service cloudprofiler"
  value       = {
    "agent" : "roles/cloudprofiler.agent",
    "user" : "roles/cloudprofiler.user"
  }
}

output "run" {
  description = "Roles of service run"
  value       = {
    "admin" : "roles/run.admin",
    "developer" : "roles/run.developer",
    "invoker" : "roles/run.invoker",
    "serviceAgent" : "roles/run.serviceAgent",
    "viewer" : "roles/run.viewer"
  }
}

output "cloudscheduler" {
  description = "Roles of service cloudscheduler"
  value       = {
    "admin" : "roles/cloudscheduler.admin",
    "jobRunner" : "roles/cloudscheduler.jobRunner",
    "serviceAgent" : "roles/cloudscheduler.serviceAgent",
    "viewer" : "roles/cloudscheduler.viewer"
  }
}

output "cloudsecurityscanner" {
  description = "Roles of service cloudsecurityscanner"
  value       = {
    "editor" : "roles/cloudsecurityscanner.editor",
    "runner" : "roles/cloudsecurityscanner.runner",
    "viewer" : "roles/cloudsecurityscanner.viewer"
  }
}

output "servicebroker" {
  description = "Roles of service servicebroker"
  value       = {
    "admin" : "roles/servicebroker.admin",
    "operator" : "roles/servicebroker.operator"
  }
}

output "spanner" {
  description = "Roles of service spanner"
  value       = {
    "admin" : "roles/spanner.admin",
    "backupAdmin" : "roles/spanner.backupAdmin",
    "backupWriter" : "roles/spanner.backupWriter",
    "databaseAdmin" : "roles/spanner.databaseAdmin",
    "databaseReader" : "roles/spanner.databaseReader",
    "databaseRoleUser" : "roles/spanner.databaseRoleUser",
    "databaseUser" : "roles/spanner.databaseUser",
    "fineGrainedAccessUser" : "roles/spanner.fineGrainedAccessUser",
    "restoreAdmin" : "roles/spanner.restoreAdmin",
    "viewer" : "roles/spanner.viewer"
  }
}

output "cloudsql" {
  description = "Roles of service cloudsql"
  value       = {
    "admin" : "roles/cloudsql.admin",
    "client" : "roles/cloudsql.client",
    "editor" : "roles/cloudsql.editor",
    "instanceUser" : "roles/cloudsql.instanceUser",
    "serviceAgent" : "roles/cloudsql.serviceAgent",
    "viewer" : "roles/cloudsql.viewer"
  }
}

output "storage" {
  description = "Roles of service storage"
  value       = {
    "admin" : "roles/storage.admin",
    "hmacKeyAdmin" : "roles/storage.hmacKeyAdmin",
    "legacyBucketOwner" : "roles/storage.legacyBucketOwner",
    "legacyBucketReader" : "roles/storage.legacyBucketReader",
    "legacyBucketWriter" : "roles/storage.legacyBucketWriter",
    "legacyObjectOwner" : "roles/storage.legacyObjectOwner",
    "legacyObjectReader" : "roles/storage.legacyObjectReader",
    "objectAdmin" : "roles/storage.objectAdmin",
    "objectCreator" : "roles/storage.objectCreator",
    "objectViewer" : "roles/storage.objectViewer"
  }
}

output "storagetransfer" {
  description = "Roles of service storagetransfer"
  value       = {
    "admin" : "roles/storagetransfer.admin",
    "serviceAgent" : "roles/storagetransfer.serviceAgent",
    "transferAgent" : "roles/storagetransfer.transferAgent",
    "user" : "roles/storagetransfer.user",
    "viewer" : "roles/storagetransfer.viewer"
  }
}

output "cloudjobdiscovery" {
  description = "Roles of service cloudjobdiscovery"
  value       = {
    "admin" : "roles/cloudjobdiscovery.admin",
    "jobsEditor" : "roles/cloudjobdiscovery.jobsEditor",
    "jobsViewer" : "roles/cloudjobdiscovery.jobsViewer",
    "profilesEditor" : "roles/cloudjobdiscovery.profilesEditor",
    "profilesViewer" : "roles/cloudjobdiscovery.profilesViewer"
  }
}

output "cloudtasks" {
  description = "Roles of service cloudtasks"
  value       = {
    "admin" : "roles/cloudtasks.admin",
    "enqueuer" : "roles/cloudtasks.enqueuer",
    "queueAdmin" : "roles/cloudtasks.queueAdmin",
    "serviceAgent" : "roles/cloudtasks.serviceAgent",
    "taskDeleter" : "roles/cloudtasks.taskDeleter",
    "taskRunner" : "roles/cloudtasks.taskRunner",
    "viewer" : "roles/cloudtasks.viewer"
  }
}

output "tpu" {
  description = "Roles of service tpu"
  value       = {
    "admin" : "roles/tpu.admin",
    "serviceAgent" : "roles/tpu.serviceAgent",
    "viewer" : "roles/tpu.viewer",
    "xpnAgent" : "roles/tpu.xpnAgent"
  }
}

output "cloudtrace" {
  description = "Roles of service cloudtrace"
  value       = {
    "admin" : "roles/cloudtrace.admin",
    "agent" : "roles/cloudtrace.agent",
    "user" : "roles/cloudtrace.user"
  }
}

output "cloudtranslate" {
  description = "Roles of service cloudtranslate"
  value       = {
    "admin" : "roles/cloudtranslate.admin",
    "editor" : "roles/cloudtranslate.editor",
    "serviceAgent" : "roles/cloudtranslate.serviceAgent",
    "user" : "roles/cloudtranslate.user",
    "viewer" : "roles/cloudtranslate.viewer"
  }
}

output "workstations" {
  description = "Roles of service workstations"
  value       = {
    "admin" : "roles/workstations.admin",
    "networkAdmin" : "roles/workstations.networkAdmin",
    "operationViewer" : "roles/workstations.operationViewer",
    "user" : "roles/workstations.user",
    "workstationCreator" : "roles/workstations.workstationCreator",
    "workstationUser" : "roles/workstations.workstationUser"
  }
}

output "compute" {
  description = "Roles of service compute"
  value       = {
    "admin" : "roles/compute.admin",
    "imageUser" : "roles/compute.imageUser",
    "instanceAdmin" : "roles/compute.instanceAdmin",
    "instanceAdmin_v1" : "roles/compute.instanceAdmin.v1",
    "loadBalancerAdmin" : "roles/compute.loadBalancerAdmin",
    "loadBalancerServiceUser" : "roles/compute.loadBalancerServiceUser",
    "networkAdmin" : "roles/compute.networkAdmin",
    "networkUser" : "roles/compute.networkUser",
    "networkViewer" : "roles/compute.networkViewer",
    "orgFirewallPolicyAdmin" : "roles/compute.orgFirewallPolicyAdmin",
    "orgFirewallPolicyUser" : "roles/compute.orgFirewallPolicyUser",
    "orgSecurityPolicyAdmin" : "roles/compute.orgSecurityPolicyAdmin",
    "orgSecurityPolicyUser" : "roles/compute.orgSecurityPolicyUser",
    "orgSecurityResourceAdmin" : "roles/compute.orgSecurityResourceAdmin",
    "osAdminLogin" : "roles/compute.osAdminLogin",
    "osLogin" : "roles/compute.osLogin",
    "osLoginExternalUser" : "roles/compute.osLoginExternalUser",
    "packetMirroringAdmin" : "roles/compute.packetMirroringAdmin",
    "packetMirroringUser" : "roles/compute.packetMirroringUser",
    "publicIpAdmin" : "roles/compute.publicIpAdmin",
    "securityAdmin" : "roles/compute.securityAdmin",
    "serviceAgent" : "roles/compute.serviceAgent",
    "soleTenantViewer" : "roles/compute.soleTenantViewer",
    "storageAdmin" : "roles/compute.storageAdmin",
    "viewer" : "roles/compute.viewer",
    "xpnAdmin" : "roles/compute.xpnAdmin"
  }
}

output "osconfig" {
  description = "Roles of service osconfig"
  value       = {
    "guestPolicyAdmin" : "roles/osconfig.guestPolicyAdmin",
    "guestPolicyEditor" : "roles/osconfig.guestPolicyEditor",
    "guestPolicyViewer" : "roles/osconfig.guestPolicyViewer",
    "instanceOSPoliciesComplianceViewer" : "roles/osconfig.instanceOSPoliciesComplianceViewer",
    "inventoryViewer" : "roles/osconfig.inventoryViewer",
    "osPolicyAssignmentAdmin" : "roles/osconfig.osPolicyAssignmentAdmin",
    "osPolicyAssignmentEditor" : "roles/osconfig.osPolicyAssignmentEditor",
    "osPolicyAssignmentReportViewer" : "roles/osconfig.osPolicyAssignmentReportViewer",
    "osPolicyAssignmentViewer" : "roles/osconfig.osPolicyAssignmentViewer",
    "patchDeploymentAdmin" : "roles/osconfig.patchDeploymentAdmin",
    "patchDeploymentViewer" : "roles/osconfig.patchDeploymentViewer",
    "patchJobExecutor" : "roles/osconfig.patchJobExecutor",
    "patchJobViewer" : "roles/osconfig.patchJobViewer",
    "serviceAgent" : "roles/osconfig.serviceAgent",
    "vulnerabilityReportViewer" : "roles/osconfig.vulnerabilityReportViewer"
  }
}

output "containeranalysis" {
  description = "Roles of service containeranalysis"
  value       = {
    "ServiceAgent" : "roles/containeranalysis.ServiceAgent",
    "admin" : "roles/containeranalysis.admin",
    "notes_attacher" : "roles/containeranalysis.notes.attacher",
    "notes_editor" : "roles/containeranalysis.notes.editor",
    "notes_occurrences_viewer" : "roles/containeranalysis.notes.occurrences.viewer",
    "notes_viewer" : "roles/containeranalysis.notes.viewer",
    "occurrences_editor" : "roles/containeranalysis.occurrences.editor",
    "occurrences_viewer" : "roles/containeranalysis.occurrences.viewer"
  }
}

output "datacatalog" {
  description = "Roles of service datacatalog"
  value       = {
    "admin" : "roles/datacatalog.admin",
    "categoryAdmin" : "roles/datacatalog.categoryAdmin",
    "categoryFineGrainedReader" : "roles/datacatalog.categoryFineGrainedReader",
    "dataSteward" : "roles/datacatalog.dataSteward",
    "entryGroupCreator" : "roles/datacatalog.entryGroupCreator",
    "entryGroupOwner" : "roles/datacatalog.entryGroupOwner",
    "entryOwner" : "roles/datacatalog.entryOwner",
    "entryViewer" : "roles/datacatalog.entryViewer",
    "tagEditor" : "roles/datacatalog.tagEditor",
    "tagTemplateCreator" : "roles/datacatalog.tagTemplateCreator",
    "tagTemplateOwner" : "roles/datacatalog.tagTemplateOwner",
    "tagTemplateUser" : "roles/datacatalog.tagTemplateUser",
    "tagTemplateViewer" : "roles/datacatalog.tagTemplateViewer",
    "viewer" : "roles/datacatalog.viewer"
  }
}

output "dataconnectors" {
  description = "Roles of service dataconnectors"
  value       = {
    "connectorAdmin" : "roles/dataconnectors.connectorAdmin",
    "connectorUser" : "roles/dataconnectors.connectorUser",
    "serviceAgent" : "roles/dataconnectors.serviceAgent"
  }
}

output "datamigration" {
  description = "Roles of service datamigration"
  value       = {
    "admin" : "roles/datamigration.admin"
  }
}

output "datapipelines" {
  description = "Roles of service datapipelines"
  value       = {
    "admin" : "roles/datapipelines.admin",
    "invoker" : "roles/datapipelines.invoker",
    "serviceAgent" : "roles/datapipelines.serviceAgent",
    "viewer" : "roles/datapipelines.viewer"
  }
}

output "datastudio" {
  description = "Roles of service datastudio"
  value       = {
    "contentManager" : "roles/datastudio.contentManager",
    "contributor" : "roles/datastudio.contributor",
    "editor" : "roles/datastudio.editor",
    "manager" : "roles/datastudio.manager",
    "serviceAgent" : "roles/datastudio.serviceAgent",
    "viewer" : "roles/datastudio.viewer"
  }
}

output "dataflow" {
  description = "Roles of service dataflow"
  value       = {
    "admin" : "roles/dataflow.admin",
    "developer" : "roles/dataflow.developer",
    "serviceAgent" : "roles/dataflow.serviceAgent",
    "viewer" : "roles/dataflow.viewer",
    "worker" : "roles/dataflow.worker"
  }
}

output "dataform" {
  description = "Roles of service dataform"
  value       = {
    "admin" : "roles/dataform.admin",
    "editor" : "roles/dataform.editor",
    "serviceAgent" : "roles/dataform.serviceAgent",
    "viewer" : "roles/dataform.viewer"
  }
}

output "dataprep" {
  description = "Roles of service dataprep"
  value       = {
    "projects_user" : "roles/dataprep.projects.user",
    "serviceAgent" : "roles/dataprep.serviceAgent"
  }
}

output "dataproc" {
  description = "Roles of service dataproc"
  value       = {
    "admin" : "roles/dataproc.admin",
    "editor" : "roles/dataproc.editor",
    "hubAgent" : "roles/dataproc.hubAgent",
    "serviceAgent" : "roles/dataproc.serviceAgent",
    "viewer" : "roles/dataproc.viewer",
    "worker" : "roles/dataproc.worker"
  }
}

output "metastore" {
  description = "Roles of service metastore"
  value       = {
    "admin" : "roles/metastore.admin",
    "editor" : "roles/metastore.editor",
    "federationAccessor" : "roles/metastore.federationAccessor",
    "metadataEditor" : "roles/metastore.metadataEditor",
    "metadataOperator" : "roles/metastore.metadataOperator",
    "metadataOwner" : "roles/metastore.metadataOwner",
    "metadataUser" : "roles/metastore.metadataUser",
    "metadataViewer" : "roles/metastore.metadataViewer",
    "serviceAgent" : "roles/metastore.serviceAgent",
    "user" : "roles/metastore.user"
  }
}

output "datastore" {
  description = "Roles of service datastore"
  value       = {
    "importExportAdmin" : "roles/datastore.importExportAdmin",
    "indexAdmin" : "roles/datastore.indexAdmin",
    "keyVisualizerViewer" : "roles/datastore.keyVisualizerViewer",
    "owner" : "roles/datastore.owner",
    "user" : "roles/datastore.user",
    "viewer" : "roles/datastore.viewer"
  }
}

output "datastream" {
  description = "Roles of service datastream"
  value       = {
    "admin" : "roles/datastream.admin",
    "viewer" : "roles/datastream.viewer"
  }
}

output "deploymentmanager" {
  description = "Roles of service deploymentmanager"
  value       = {
    "editor" : "roles/deploymentmanager.editor",
    "typeEditor" : "roles/deploymentmanager.typeEditor",
    "typeViewer" : "roles/deploymentmanager.typeViewer",
    "viewer" : "roles/deploymentmanager.viewer"
  }
}

output "dialogflow" {
  description = "Roles of service dialogflow"
  value       = {
    "aamAdmin" : "roles/dialogflow.aamAdmin",
    "aamConversationalArchitect" : "roles/dialogflow.aamConversationalArchitect",
    "aamDialogDesigner" : "roles/dialogflow.aamDialogDesigner",
    "aamLeadDialogDesigner" : "roles/dialogflow.aamLeadDialogDesigner",
    "aamViewer" : "roles/dialogflow.aamViewer",
    "admin" : "roles/dialogflow.admin",
    "client" : "roles/dialogflow.client",
    "consoleAgentEditor" : "roles/dialogflow.consoleAgentEditor",
    "consoleSimulatorUser" : "roles/dialogflow.consoleSimulatorUser",
    "consoleSmartMessagingAllowlistEditor" : "roles/dialogflow.consoleSmartMessagingAllowlistEditor",
    "conversationManager" : "roles/dialogflow.conversationManager",
    "entityTypeAdmin" : "roles/dialogflow.entityTypeAdmin",
    "environmentEditor" : "roles/dialogflow.environmentEditor",
    "flowEditor" : "roles/dialogflow.flowEditor",
    "integrationManager" : "roles/dialogflow.integrationManager",
    "intentAdmin" : "roles/dialogflow.intentAdmin",
    "reader" : "roles/dialogflow.reader",
    "serviceAgent" : "roles/dialogflow.serviceAgent",
    "webhookAdmin" : "roles/dialogflow.webhookAdmin"
  }
}

output "dns" {
  description = "Roles of service dns"
  value       = {
    "admin" : "roles/dns.admin",
    "peer" : "roles/dns.peer",
    "reader" : "roles/dns.reader"
  }
}

output "documentai" {
  description = "Roles of service documentai"
  value       = {
    "admin" : "roles/documentai.admin",
    "apiUser" : "roles/documentai.apiUser",
    "editor" : "roles/documentai.editor",
    "viewer" : "roles/documentai.viewer"
  }
}

output "earthengine" {
  description = "Roles of service earthengine"
  value       = {
    "admin" : "roles/earthengine.admin",
    "appsPublisher" : "roles/earthengine.appsPublisher",
    "viewer" : "roles/earthengine.viewer",
    "writer" : "roles/earthengine.writer"
  }
}

output "edgecontainer" {
  description = "Roles of service edgecontainer"
  value       = {
    "admin" : "roles/edgecontainer.admin",
    "machineUser" : "roles/edgecontainer.machineUser",
    "viewer" : "roles/edgecontainer.viewer"
  }
}

output "endpoints" {
  description = "Roles of service endpoints"
  value       = {
    "portalAdmin" : "roles/endpoints.portalAdmin",
    "serviceAgent" : "roles/endpoints.serviceAgent"
  }
}

output "errorreporting" {
  description = "Roles of service errorreporting"
  value       = {
    "admin" : "roles/errorreporting.admin",
    "user" : "roles/errorreporting.user",
    "viewer" : "roles/errorreporting.viewer",
    "writer" : "roles/errorreporting.writer"
  }
}

output "eventarc" {
  description = "Roles of service eventarc"
  value       = {
    "admin" : "roles/eventarc.admin",
    "connectionPublisher" : "roles/eventarc.connectionPublisher",
    "developer" : "roles/eventarc.developer",
    "eventReceiver" : "roles/eventarc.eventReceiver",
    "publisher" : "roles/eventarc.publisher",
    "serviceAgent" : "roles/eventarc.serviceAgent",
    "viewer" : "roles/eventarc.viewer"
  }
}

output "firebase" {
  description = "Roles of service firebase"
  value       = {
    "admin" : "roles/firebase.admin",
    "analyticsAdmin" : "roles/firebase.analyticsAdmin",
    "analyticsViewer" : "roles/firebase.analyticsViewer",
    "appDistributionSdkServiceAgent" : "roles/firebase.appDistributionSdkServiceAgent",
    "developAdmin" : "roles/firebase.developAdmin",
    "developViewer" : "roles/firebase.developViewer",
    "growthAdmin" : "roles/firebase.growthAdmin",
    "growthViewer" : "roles/firebase.growthViewer",
    "managementServiceAgent" : "roles/firebase.managementServiceAgent",
    "qualityAdmin" : "roles/firebase.qualityAdmin",
    "qualityViewer" : "roles/firebase.qualityViewer",
    "sdkAdminServiceAgent" : "roles/firebase.sdkAdminServiceAgent",
    "sdkProvisioningServiceAgent" : "roles/firebase.sdkProvisioningServiceAgent",
    "viewer" : "roles/firebase.viewer"
  }
}

output "cloudconfig" {
  description = "Roles of service cloudconfig"
  value       = {
    "admin" : "roles/cloudconfig.admin",
    "viewer" : "roles/cloudconfig.viewer"
  }
}

output "cloudtestservice" {
  description = "Roles of service cloudtestservice"
  value       = {
    "testAdmin" : "roles/cloudtestservice.testAdmin",
    "testViewer" : "roles/cloudtestservice.testViewer"
  }
}

output "firebaseabt" {
  description = "Roles of service firebaseabt"
  value       = {
    "admin" : "roles/firebaseabt.admin",
    "viewer" : "roles/firebaseabt.viewer"
  }
}

output "firebaseappcheck" {
  description = "Roles of service firebaseappcheck"
  value       = {
    "admin" : "roles/firebaseappcheck.admin",
    "serviceAgent" : "roles/firebaseappcheck.serviceAgent",
    "viewer" : "roles/firebaseappcheck.viewer"
  }
}

output "firebaseappdistro" {
  description = "Roles of service firebaseappdistro"
  value       = {
    "admin" : "roles/firebaseappdistro.admin",
    "viewer" : "roles/firebaseappdistro.viewer"
  }
}

output "firebaseauth" {
  description = "Roles of service firebaseauth"
  value       = {
    "admin" : "roles/firebaseauth.admin",
    "viewer" : "roles/firebaseauth.viewer"
  }
}

output "firebasecrashlytics" {
  description = "Roles of service firebasecrashlytics"
  value       = {
    "admin" : "roles/firebasecrashlytics.admin",
    "viewer" : "roles/firebasecrashlytics.viewer"
  }
}

output "firebasedatabase" {
  description = "Roles of service firebasedatabase"
  value       = {
    "admin" : "roles/firebasedatabase.admin",
    "viewer" : "roles/firebasedatabase.viewer"
  }
}

output "firebasedynamiclinks" {
  description = "Roles of service firebasedynamiclinks"
  value       = {
    "admin" : "roles/firebasedynamiclinks.admin",
    "viewer" : "roles/firebasedynamiclinks.viewer"
  }
}

output "firebasehosting" {
  description = "Roles of service firebasehosting"
  value       = {
    "admin" : "roles/firebasehosting.admin",
    "viewer" : "roles/firebasehosting.viewer"
  }
}

output "firebaseinappmessaging" {
  description = "Roles of service firebaseinappmessaging"
  value       = {
    "admin" : "roles/firebaseinappmessaging.admin",
    "viewer" : "roles/firebaseinappmessaging.viewer"
  }
}

output "firebasemessagingcampaigns" {
  description = "Roles of service firebasemessagingcampaigns"
  value       = {
    "admin" : "roles/firebasemessagingcampaigns.admin",
    "viewer" : "roles/firebasemessagingcampaigns.viewer"
  }
}

output "firebaseml" {
  description = "Roles of service firebaseml"
  value       = {
    "admin" : "roles/firebaseml.admin",
    "viewer" : "roles/firebaseml.viewer"
  }
}

output "firebasenotifications" {
  description = "Roles of service firebasenotifications"
  value       = {
    "admin" : "roles/firebasenotifications.admin",
    "viewer" : "roles/firebasenotifications.viewer"
  }
}

output "firebaseperformance" {
  description = "Roles of service firebaseperformance"
  value       = {
    "admin" : "roles/firebaseperformance.admin",
    "viewer" : "roles/firebaseperformance.viewer"
  }
}

output "firebaserules" {
  description = "Roles of service firebaserules"
  value       = {
    "admin" : "roles/firebaserules.admin",
    "viewer" : "roles/firebaserules.viewer"
  }
}

output "firebasestorage" {
  description = "Roles of service firebasestorage"
  value       = {
    "admin" : "roles/firebasestorage.admin",
    "serviceAgent" : "roles/firebasestorage.serviceAgent",
    "viewer" : "roles/firebasestorage.viewer"
  }
}

output "fleetengine" {
  description = "Roles of service fleetengine"
  value       = {
    "consumerSdkUser" : "roles/fleetengine.consumerSdkUser",
    "deliveryConsumer" : "roles/fleetengine.deliveryConsumer",
    "deliveryFleetReader" : "roles/fleetengine.deliveryFleetReader",
    "deliverySuperUser" : "roles/fleetengine.deliverySuperUser",
    "deliveryTrustedDriver" : "roles/fleetengine.deliveryTrustedDriver",
    "deliveryUntrustedDriver" : "roles/fleetengine.deliveryUntrustedDriver",
    "driverSdkUser" : "roles/fleetengine.driverSdkUser",
    "serviceAgent" : "roles/fleetengine.serviceAgent",
    "serviceSuperUser" : "roles/fleetengine.serviceSuperUser"
  }
}

output "genomics" {
  description = "Roles of service genomics"
  value       = {
    "admin" : "roles/genomics.admin",
    "editor" : "roles/genomics.editor",
    "pipelinesRunner" : "roles/genomics.pipelinesRunner",
    "serviceAgent" : "roles/genomics.serviceAgent",
    "viewer" : "roles/genomics.viewer"
  }
}

output "gkehub" {
  description = "Roles of service gkehub"
  value       = {
    "admin" : "roles/gkehub.admin",
    "connect" : "roles/gkehub.connect",
    "editor" : "roles/gkehub.editor",
    "gatewayAdmin" : "roles/gkehub.gatewayAdmin",
    "gatewayEditor" : "roles/gkehub.gatewayEditor",
    "gatewayReader" : "roles/gkehub.gatewayReader",
    "serviceAgent" : "roles/gkehub.serviceAgent",
    "viewer" : "roles/gkehub.viewer"
  }
}

output "gkeonprem" {
  description = "Roles of service gkeonprem"
  value       = {
    "admin" : "roles/gkeonprem.admin",
    "viewer" : "roles/gkeonprem.viewer"
  }
}

output "gsuiteaddons" {
  description = "Roles of service gsuiteaddons"
  value       = {
    "developer" : "roles/gsuiteaddons.developer",
    "reader" : "roles/gsuiteaddons.reader",
    "tester" : "roles/gsuiteaddons.tester"
  }
}

output "chat" {
  description = "Roles of service chat"
  value       = {
    "owner" : "roles/chat.owner",
    "reader" : "roles/chat.reader"
  }
}

output "iam" {
  description = "Roles of service iam"
  value       = {
    "denyAdmin" : "roles/iam.denyAdmin",
    "denyReviewer" : "roles/iam.denyReviewer",
    "organizationRoleAdmin" : "roles/iam.organizationRoleAdmin",
    "organizationRoleViewer" : "roles/iam.organizationRoleViewer",
    "roleAdmin" : "roles/iam.roleAdmin",
    "roleViewer" : "roles/iam.roleViewer",
    "securityAdmin" : "roles/iam.securityAdmin",
    "securityReviewer" : "roles/iam.securityReviewer",
    "serviceAccountAdmin" : "roles/iam.serviceAccountAdmin",
    "serviceAccountCreator" : "roles/iam.serviceAccountCreator",
    "serviceAccountDeleter" : "roles/iam.serviceAccountDeleter",
    "serviceAccountKeyAdmin" : "roles/iam.serviceAccountKeyAdmin",
    "serviceAccountOpenIdTokenCreator" : "roles/iam.serviceAccountOpenIdTokenCreator",
    "serviceAccountTokenCreator" : "roles/iam.serviceAccountTokenCreator",
    "serviceAccountUser" : "roles/iam.serviceAccountUser",
    "serviceAccountViewer" : "roles/iam.serviceAccountViewer",
    "workforcePoolAdmin" : "roles/iam.workforcePoolAdmin",
    "workforcePoolEditor" : "roles/iam.workforcePoolEditor",
    "workforcePoolViewer" : "roles/iam.workforcePoolViewer",
    "workloadIdentityPoolAdmin" : "roles/iam.workloadIdentityPoolAdmin",
    "workloadIdentityPoolViewer" : "roles/iam.workloadIdentityPoolViewer",
    "workloadIdentityUser" : "roles/iam.workloadIdentityUser"
  }
}

output "krmapihosting" {
  description = "Roles of service krmapihosting"
  value       = {
    "admin" : "roles/krmapihosting.admin",
    "viewer" : "roles/krmapihosting.viewer"
  }
}

output "container" {
  description = "Roles of service container"
  value       = {
    "admin" : "roles/container.admin",
    "clusterAdmin" : "roles/container.clusterAdmin",
    "clusterViewer" : "roles/container.clusterViewer",
    "developer" : "roles/container.developer",
    "hostServiceAgentUser" : "roles/container.hostServiceAgentUser",
    "nodeServiceAccount" : "roles/container.nodeServiceAccount",
    "nodeServiceAgent" : "roles/container.nodeServiceAgent",
    "serviceAgent" : "roles/container.serviceAgent",
    "viewer" : "roles/container.viewer"
  }
}

output "livestream" {
  description = "Roles of service livestream"
  value       = {
    "editor" : "roles/livestream.editor",
    "serviceAgent" : "roles/livestream.serviceAgent",
    "viewer" : "roles/livestream.viewer"
  }
}

output "logging" {
  description = "Roles of service logging"
  value       = {
    "admin" : "roles/logging.admin",
    "bucketWriter" : "roles/logging.bucketWriter",
    "configWriter" : "roles/logging.configWriter",
    "fieldAccessor" : "roles/logging.fieldAccessor",
    "linkViewer" : "roles/logging.linkViewer",
    "logWriter" : "roles/logging.logWriter",
    "privateLogViewer" : "roles/logging.privateLogViewer",
    "serviceAgent" : "roles/logging.serviceAgent",
    "viewAccessor" : "roles/logging.viewAccessor",
    "viewer" : "roles/logging.viewer"
  }
}

output "mapsadmin" {
  description = "Roles of service mapsadmin"
  value       = {
    "admin" : "roles/mapsadmin.admin",
    "viewer" : "roles/mapsadmin.viewer"
  }
}

output "memcache" {
  description = "Roles of service memcache"
  value       = {
    "admin" : "roles/memcache.admin",
    "editor" : "roles/memcache.editor",
    "serviceAgent" : "roles/memcache.serviceAgent",
    "viewer" : "roles/memcache.viewer"
  }
}

output "redis" {
  description = "Roles of service redis"
  value       = {
    "admin" : "roles/redis.admin",
    "editor" : "roles/redis.editor",
    "serviceAgent" : "roles/redis.serviceAgent",
    "viewer" : "roles/redis.viewer"
  }
}

output "meshconfig" {
  description = "Roles of service meshconfig"
  value       = {
    "admin" : "roles/meshconfig.admin",
    "serviceAgent" : "roles/meshconfig.serviceAgent",
    "viewer" : "roles/meshconfig.viewer"
  }
}

output "migrationcenter" {
  description = "Roles of service migrationcenter"
  value       = {
    "admin" : "roles/migrationcenter.admin",
    "serviceAgent" : "roles/migrationcenter.serviceAgent",
    "viewer" : "roles/migrationcenter.viewer"
  }
}

output "monitoring" {
  description = "Roles of service monitoring"
  value       = {
    "admin" : "roles/monitoring.admin",
    "alertPolicyEditor" : "roles/monitoring.alertPolicyEditor",
    "alertPolicyViewer" : "roles/monitoring.alertPolicyViewer",
    "dashboardEditor" : "roles/monitoring.dashboardEditor",
    "dashboardViewer" : "roles/monitoring.dashboardViewer",
    "editor" : "roles/monitoring.editor",
    "metricWriter" : "roles/monitoring.metricWriter",
    "metricsScopesAdmin" : "roles/monitoring.metricsScopesAdmin",
    "metricsScopesViewer" : "roles/monitoring.metricsScopesViewer",
    "notificationChannelEditor" : "roles/monitoring.notificationChannelEditor",
    "notificationChannelViewer" : "roles/monitoring.notificationChannelViewer",
    "notificationServiceAgent" : "roles/monitoring.notificationServiceAgent",
    "servicesEditor" : "roles/monitoring.servicesEditor",
    "servicesViewer" : "roles/monitoring.servicesViewer",
    "uptimeCheckConfigEditor" : "roles/monitoring.uptimeCheckConfigEditor",
    "uptimeCheckConfigViewer" : "roles/monitoring.uptimeCheckConfigViewer",
    "viewer" : "roles/monitoring.viewer"
  }
}

output "networkconnectivity" {
  description = "Roles of service networkconnectivity"
  value       = {
    "hubAdmin" : "roles/networkconnectivity.hubAdmin",
    "hubViewer" : "roles/networkconnectivity.hubViewer",
    "spokeAdmin" : "roles/networkconnectivity.spokeAdmin"
  }
}

output "networkmanagement" {
  description = "Roles of service networkmanagement"
  value       = {
    "admin" : "roles/networkmanagement.admin",
    "serviceAgent" : "roles/networkmanagement.serviceAgent",
    "viewer" : "roles/networkmanagement.viewer"
  }
}

output "ondemandscanning" {
  description = "Roles of service ondemandscanning"
  value       = {
    "admin" : "roles/ondemandscanning.admin"
  }
}

output "opsconfigmonitoring" {
  description = "Roles of service opsconfigmonitoring"
  value       = {
    "resourceMetadata_viewer" : "roles/opsconfigmonitoring.resourceMetadata.viewer",
    "resourceMetadata_writer" : "roles/opsconfigmonitoring.resourceMetadata.writer"
  }
}

output "axt" {
  description = "Roles of service axt"
  value       = {
    "admin" : "roles/axt.admin"
  }
}

output "orgpolicy" {
  description = "Roles of service orgpolicy"
  value       = {
    "policyAdmin" : "roles/orgpolicy.policyAdmin",
    "policyViewer" : "roles/orgpolicy.policyViewer"
  }
}

output "advisorynotifications" {
  description = "Roles of service advisorynotifications"
  value       = {
    "viewer" : "roles/advisorynotifications.viewer"
  }
}

output "autoscaling" {
  description = "Roles of service autoscaling"
  value       = {
    "metricsWriter" : "roles/autoscaling.metricsWriter",
    "recommendationsReader" : "roles/autoscaling.recommendationsReader",
    "sitesAdmin" : "roles/autoscaling.sitesAdmin",
    "stateWriter" : "roles/autoscaling.stateWriter"
  }
}

output "baremetalsolution" {
  description = "Roles of service baremetalsolution"
  value       = {
    "admin" : "roles/baremetalsolution.admin",
    "editor" : "roles/baremetalsolution.editor",
    "instancesadmin" : "roles/baremetalsolution.instancesadmin",
    "instancesviewer" : "roles/baremetalsolution.instancesviewer",
    "lunsadmin" : "roles/baremetalsolution.lunsadmin",
    "lunsviewer" : "roles/baremetalsolution.lunsviewer",
    "networksadmin" : "roles/baremetalsolution.networksadmin",
    "nfssharesadmin" : "roles/baremetalsolution.nfssharesadmin",
    "nfsshareseditor" : "roles/baremetalsolution.nfsshareseditor",
    "nfssharesviewer" : "roles/baremetalsolution.nfssharesviewer",
    "storageadmin" : "roles/baremetalsolution.storageadmin",
    "viewer" : "roles/baremetalsolution.viewer",
    "volumesadmin" : "roles/baremetalsolution.volumesadmin",
    "volumeseditor" : "roles/baremetalsolution.volumeseditor",
    "volumesnapshotsadmin" : "roles/baremetalsolution.volumesnapshotsadmin",
    "volumesnapshotseditor" : "roles/baremetalsolution.volumesnapshotseditor",
    "volumesnapshotsviewer" : "roles/baremetalsolution.volumesnapshotsviewer",
    "volumessviewer" : "roles/baremetalsolution.volumessviewer"
  }
}

output "batch" {
  description = "Roles of service batch"
  value       = {
    "agentReporter" : "roles/batch.agentReporter",
    "jobsAdmin" : "roles/batch.jobsAdmin",
    "jobsEditor" : "roles/batch.jobsEditor",
    "jobsViewer" : "roles/batch.jobsViewer",
    "serviceAgent" : "roles/batch.serviceAgent"
  }
}

output "bigquerymigration" {
  description = "Roles of service bigquerymigration"
  value       = {
    "editor" : "roles/bigquerymigration.editor",
    "orchestrator" : "roles/bigquerymigration.orchestrator",
    "translationUser" : "roles/bigquerymigration.translationUser",
    "viewer" : "roles/bigquerymigration.viewer",
    "worker" : "roles/bigquerymigration.worker"
  }
}

output "carestudio" {
  description = "Roles of service carestudio"
  value       = {
    "viewer" : "roles/carestudio.viewer"
  }
}

output "chronicle" {
  description = "Roles of service chronicle"
  value       = {
    "admin" : "roles/chronicle.admin",
    "viewer" : "roles/chronicle.viewer"
  }
}

output "chroniclesm" {
  description = "Roles of service chroniclesm"
  value       = {
    "admin" : "roles/chroniclesm.admin",
    "viewer" : "roles/chroniclesm.viewer"
  }
}

output "cloudoptimization" {
  description = "Roles of service cloudoptimization"
  value       = {
    "admin" : "roles/cloudoptimization.admin",
    "editor" : "roles/cloudoptimization.editor",
    "serviceAgent" : "roles/cloudoptimization.serviceAgent",
    "viewer" : "roles/cloudoptimization.viewer"
  }
}

output "contactcenteraiplatform" {
  description = "Roles of service contactcenteraiplatform"
  value       = {
    "admin" : "roles/contactcenteraiplatform.admin",
    "viewer" : "roles/contactcenteraiplatform.viewer"
  }
}

output "contactcenterinsights" {
  description = "Roles of service contactcenterinsights"
  value       = {
    "editor" : "roles/contactcenterinsights.editor",
    "serviceAgent" : "roles/contactcenterinsights.serviceAgent",
    "viewer" : "roles/contactcenterinsights.viewer"
  }
}

output "containersecurity" {
  description = "Roles of service containersecurity"
  value       = {
    "viewer" : "roles/containersecurity.viewer"
  }
}

output "contentwarehouse" {
  description = "Roles of service contentwarehouse"
  value       = {
    "admin" : "roles/contentwarehouse.admin",
    "documentAdmin" : "roles/contentwarehouse.documentAdmin",
    "documentCreator" : "roles/contentwarehouse.documentCreator",
    "documentEditor" : "roles/contentwarehouse.documentEditor",
    "documentSchemaViewer" : "roles/contentwarehouse.documentSchemaViewer",
    "documentViewer" : "roles/contentwarehouse.documentViewer",
    "serviceAgent" : "roles/contentwarehouse.serviceAgent"
  }
}

output "dataprocessing" {
  description = "Roles of service dataprocessing"
  value       = {
    "admin" : "roles/dataprocessing.admin",
    "dataSourceManager" : "roles/dataprocessing.dataSourceManager"
  }
}

output "earlyaccesscenter" {
  description = "Roles of service earlyaccesscenter"
  value       = {
    "admin" : "roles/earlyaccesscenter.admin",
    "viewer" : "roles/earlyaccesscenter.viewer"
  }
}

output "enterpriseknowledgegraph" {
  description = "Roles of service enterpriseknowledgegraph"
  value       = {
    "admin" : "roles/enterpriseknowledgegraph.admin",
    "editor" : "roles/enterpriseknowledgegraph.editor",
    "serviceAgent" : "roles/enterpriseknowledgegraph.serviceAgent",
    "viewer" : "roles/enterpriseknowledgegraph.viewer"
  }
}

output "essentialcontacts" {
  description = "Roles of service essentialcontacts"
  value       = {
    "admin" : "roles/essentialcontacts.admin",
    "viewer" : "roles/essentialcontacts.viewer"
  }
}

output "firebasecloudmessaging" {
  description = "Roles of service firebasecloudmessaging"
  value       = {
    "admin" : "roles/firebasecloudmessaging.admin"
  }
}

output "firebasecrash" {
  description = "Roles of service firebasecrash"
  value       = {
    "symbolMappingsAdmin" : "roles/firebasecrash.symbolMappingsAdmin"
  }
}

output "identityplatform" {
  description = "Roles of service identityplatform"
  value       = {
    "admin" : "roles/identityplatform.admin",
    "viewer" : "roles/identityplatform.viewer"
  }
}

output "identitytoolkit" {
  description = "Roles of service identitytoolkit"
  value       = {
    "admin" : "roles/identitytoolkit.admin",
    "viewer" : "roles/identitytoolkit.viewer"
  }
}

output "integrations" {
  description = "Roles of service integrations"
  value       = {
    "apigeeIntegrationAdminRole" : "roles/integrations.apigeeIntegrationAdminRole",
    "apigeeIntegrationDeployerRole" : "roles/integrations.apigeeIntegrationDeployerRole",
    "apigeeIntegrationEditorRole" : "roles/integrations.apigeeIntegrationEditorRole",
    "apigeeIntegrationInvokerRole" : "roles/integrations.apigeeIntegrationInvokerRole",
    "apigeeIntegrationsViewer" : "roles/integrations.apigeeIntegrationsViewer",
    "apigeeSuspensionResolver" : "roles/integrations.apigeeSuspensionResolver",
    "certificateViewer" : "roles/integrations.certificateViewer",
    "integrationAdmin" : "roles/integrations.integrationAdmin",
    "integrationDeployer" : "roles/integrations.integrationDeployer",
    "integrationEditor" : "roles/integrations.integrationEditor",
    "integrationInvoker" : "roles/integrations.integrationInvoker",
    "integrationViewer" : "roles/integrations.integrationViewer",
    "securityIntegrationAdmin" : "roles/integrations.securityIntegrationAdmin",
    "serviceAgent" : "roles/integrations.serviceAgent",
    "sfdcInstanceAdmin" : "roles/integrations.sfdcInstanceAdmin",
    "sfdcInstanceEditor" : "roles/integrations.sfdcInstanceEditor",
    "sfdcInstanceViewer" : "roles/integrations.sfdcInstanceViewer",
    "suspensionResolver" : "roles/integrations.suspensionResolver"
  }
}

output "issuerswitch" {
  description = "Roles of service issuerswitch"
  value       = {
    "admin" : "roles/issuerswitch.admin",
    "resolutionsAdmin" : "roles/issuerswitch.resolutionsAdmin",
    "rulesAdmin" : "roles/issuerswitch.rulesAdmin",
    "rulesViewer" : "roles/issuerswitch.rulesViewer",
    "transactionsViewer" : "roles/issuerswitch.transactionsViewer"
  }
}

output "oauthconfig" {
  description = "Roles of service oauthconfig"
  value       = {
    "editor" : "roles/oauthconfig.editor",
    "viewer" : "roles/oauthconfig.viewer"
  }
}

output "paymentsresellersubscription" {
  description = "Roles of service paymentsresellersubscription"
  value       = {
    "partnerAdmin" : "roles/paymentsresellersubscription.partnerAdmin",
    "partnerViewer" : "roles/paymentsresellersubscription.partnerViewer",
    "productViewer" : "roles/paymentsresellersubscription.productViewer",
    "promotionViewer" : "roles/paymentsresellersubscription.promotionViewer",
    "subscriptionEditor" : "roles/paymentsresellersubscription.subscriptionEditor",
    "subscriptionViewer" : "roles/paymentsresellersubscription.subscriptionViewer"
  }
}

output "policyanalyzer" {
  description = "Roles of service policyanalyzer"
  value       = {
    "activityAnalysisViewer" : "roles/policyanalyzer.activityAnalysisViewer"
  }
}

output "policysimulator" {
  description = "Roles of service policysimulator"
  value       = {
    "admin" : "roles/policysimulator.admin"
  }
}

output "publicca" {
  description = "Roles of service publicca"
  value       = {
    "externalAccountKeyCreator" : "roles/publicca.externalAccountKeyCreator"
  }
}

output "recommender" {
  description = "Roles of service recommender"
  value       = {
    "bigQueryCapacityCommitmentsAdmin" : "roles/recommender.bigQueryCapacityCommitmentsAdmin",
    "bigQueryCapacityCommitmentsBillingAccountAdmin" : "roles/recommender.bigQueryCapacityCommitmentsBillingAccountAdmin",
    "bigQueryCapacityCommitmentsBillingAccountViewer" : "roles/recommender.bigQueryCapacityCommitmentsBillingAccountViewer",
    "bigQueryCapacityCommitmentsProjectAdmin" : "roles/recommender.bigQueryCapacityCommitmentsProjectAdmin",
    "bigQueryCapacityCommitmentsProjectViewer" : "roles/recommender.bigQueryCapacityCommitmentsProjectViewer",
    "bigQueryCapacityCommitmentsViewer" : "roles/recommender.bigQueryCapacityCommitmentsViewer",
    "billingAccountCudAdmin" : "roles/recommender.billingAccountCudAdmin",
    "billingAccountCudViewer" : "roles/recommender.billingAccountCudViewer",
    "cloudAssetInsightsAdmin" : "roles/recommender.cloudAssetInsightsAdmin",
    "cloudAssetInsightsViewer" : "roles/recommender.cloudAssetInsightsViewer",
    "cloudsqlAdmin" : "roles/recommender.cloudsqlAdmin",
    "cloudsqlViewer" : "roles/recommender.cloudsqlViewer",
    "computeAdmin" : "roles/recommender.computeAdmin",
    "computeViewer" : "roles/recommender.computeViewer",
    "containerDiagnosisAdmin" : "roles/recommender.containerDiagnosisAdmin",
    "containerDiagnosisViewer" : "roles/recommender.containerDiagnosisViewer",
    "dataflowDiagnosticsAdmin" : "roles/recommender.dataflowDiagnosticsAdmin",
    "dataflowDiagnosticsViewer" : "roles/recommender.dataflowDiagnosticsViewer",
    "errorReportingAdmin" : "roles/recommender.errorReportingAdmin",
    "errorReportingViewer" : "roles/recommender.errorReportingViewer",
    "exporter" : "roles/recommender.exporter",
    "firewallAdmin" : "roles/recommender.firewallAdmin",
    "firewallViewer" : "roles/recommender.firewallViewer",
    "gmpAdmin" : "roles/recommender.gmpAdmin",
    "gmpViewer" : "roles/recommender.gmpViewer",
    "iamAdmin" : "roles/recommender.iamAdmin",
    "iamViewer" : "roles/recommender.iamViewer",
    "networkAnalyzerAdmin" : "roles/recommender.networkAnalyzerAdmin",
    "networkAnalyzerCloudSqlAdmin" : "roles/recommender.networkAnalyzerCloudSqlAdmin",
    "networkAnalyzerCloudSqlViewer" : "roles/recommender.networkAnalyzerCloudSqlViewer",
    "networkAnalyzerDynamicRouteAdmin" : "roles/recommender.networkAnalyzerDynamicRouteAdmin",
    "networkAnalyzerDynamicRouteViewer" : "roles/recommender.networkAnalyzerDynamicRouteViewer",
    "networkAnalyzerGkeConnectivityAdmin" : "roles/recommender.networkAnalyzerGkeConnectivityAdmin",
    "networkAnalyzerGkeConnectivityViewer" : "roles/recommender.networkAnalyzerGkeConnectivityViewer",
    "networkAnalyzerGkeIpAddressAdmin" : "roles/recommender.networkAnalyzerGkeIpAddressAdmin",
    "networkAnalyzerGkeIpAddressViewer" : "roles/recommender.networkAnalyzerGkeIpAddressViewer",
    "networkAnalyzerIpAddressAdmin" : "roles/recommender.networkAnalyzerIpAddressAdmin",
    "networkAnalyzerIpAddressViewer" : "roles/recommender.networkAnalyzerIpAddressViewer",
    "networkAnalyzerLoadBalancerAdmin" : "roles/recommender.networkAnalyzerLoadBalancerAdmin",
    "networkAnalyzerLoadBalancerViewer" : "roles/recommender.networkAnalyzerLoadBalancerViewer",
    "networkAnalyzerViewer" : "roles/recommender.networkAnalyzerViewer",
    "networkAnalyzerVpcConnectivityAdmin" : "roles/recommender.networkAnalyzerVpcConnectivityAdmin",
    "networkAnalyzerVpcConnectivityViewer" : "roles/recommender.networkAnalyzerVpcConnectivityViewer",
    "productSuggestionAdmin" : "roles/recommender.productSuggestionAdmin",
    "productSuggestionViewer" : "roles/recommender.productSuggestionViewer",
    "projectCudAdmin" : "roles/recommender.projectCudAdmin",
    "projectCudViewer" : "roles/recommender.projectCudViewer",
    "projectUtilAdmin" : "roles/recommender.projectUtilAdmin",
    "projectUtilViewer" : "roles/recommender.projectUtilViewer",
    "ucsAdmin" : "roles/recommender.ucsAdmin",
    "ucsViewer" : "roles/recommender.ucsViewer"
  }
}

output "remotebuildexecution" {
  description = "Roles of service remotebuildexecution"
  value       = {
    "actionCacheWriter" : "roles/remotebuildexecution.actionCacheWriter",
    "artifactAdmin" : "roles/remotebuildexecution.artifactAdmin",
    "artifactCreator" : "roles/remotebuildexecution.artifactCreator",
    "artifactViewer" : "roles/remotebuildexecution.artifactViewer",
    "configurationAdmin" : "roles/remotebuildexecution.configurationAdmin",
    "configurationViewer" : "roles/remotebuildexecution.configurationViewer",
    "logstreamWriter" : "roles/remotebuildexecution.logstreamWriter",
    "reservationAdmin" : "roles/remotebuildexecution.reservationAdmin",
    "serviceAgent" : "roles/remotebuildexecution.serviceAgent",
    "worker" : "roles/remotebuildexecution.worker"
  }
}

output "retail" {
  description = "Roles of service retail"
  value       = {
    "admin" : "roles/retail.admin",
    "editor" : "roles/retail.editor",
    "serviceAgent" : "roles/retail.serviceAgent",
    "viewer" : "roles/retail.viewer"
  }
}

output "runapps" {
  description = "Roles of service runapps"
  value       = {
    "developer" : "roles/runapps.developer",
    "operator" : "roles/runapps.operator",
    "serviceAgent" : "roles/runapps.serviceAgent",
    "viewer" : "roles/runapps.viewer"
  }
}

output "runtimeconfig" {
  description = "Roles of service runtimeconfig"
  value       = {
    "admin" : "roles/runtimeconfig.admin"
  }
}

output "securedlandingzone" {
  description = "Roles of service securedlandingzone"
  value       = {
    "bqdwOrgRemediator" : "roles/securedlandingzone.bqdwOrgRemediator",
    "bqdwProjectRemediator" : "roles/securedlandingzone.bqdwProjectRemediator",
    "overwatchActivator" : "roles/securedlandingzone.overwatchActivator",
    "overwatchAdmin" : "roles/securedlandingzone.overwatchAdmin",
    "overwatchViewer" : "roles/securedlandingzone.overwatchViewer",
    "serviceAgent" : "roles/securedlandingzone.serviceAgent"
  }
}

output "servicesecurityinsights" {
  description = "Roles of service servicesecurityinsights"
  value       = {
    "securityInsightsViewer" : "roles/servicesecurityinsights.securityInsightsViewer"
  }
}

output "speech" {
  description = "Roles of service speech"
  value       = {
    "admin" : "roles/speech.admin",
    "client" : "roles/speech.client",
    "editor" : "roles/speech.editor",
    "serviceAgent" : "roles/speech.serviceAgent"
  }
}

output "subscribewithgoogledeveloper" {
  description = "Roles of service subscribewithgoogledeveloper"
  value       = {
    "developer" : "roles/subscribewithgoogledeveloper.developer"
  }
}

output "timeseriesinsights" {
  description = "Roles of service timeseriesinsights"
  value       = {
    "datasetsEditor" : "roles/timeseriesinsights.datasetsEditor",
    "datasetsOwner" : "roles/timeseriesinsights.datasetsOwner",
    "datasetsViewer" : "roles/timeseriesinsights.datasetsViewer"
  }
}

output "trafficdirector" {
  description = "Roles of service trafficdirector"
  value       = {
    "client" : "roles/trafficdirector.client"
  }
}

output "translationhub" {
  description = "Roles of service translationhub"
  value       = {
    "admin" : "roles/translationhub.admin",
    "portalUser" : "roles/translationhub.portalUser"
  }
}

output "visionai" {
  description = "Roles of service visionai"
  value       = {
    "admin" : "roles/visionai.admin",
    "analysisEditor" : "roles/visionai.analysisEditor",
    "analysisViewer" : "roles/visionai.analysisViewer",
    "applicationEditor" : "roles/visionai.applicationEditor",
    "applicationViewer" : "roles/visionai.applicationViewer",
    "assetCreator" : "roles/visionai.assetCreator",
    "clusterEditor" : "roles/visionai.clusterEditor",
    "clusterViewer" : "roles/visionai.clusterViewer",
    "corpusAdmin" : "roles/visionai.corpusAdmin",
    "corpusEditor" : "roles/visionai.corpusEditor",
    "corpusViewer" : "roles/visionai.corpusViewer",
    "corpusWriter" : "roles/visionai.corpusWriter",
    "editor" : "roles/visionai.editor",
    "eventEditor" : "roles/visionai.eventEditor",
    "eventViewer" : "roles/visionai.eventViewer",
    "operatorEditor" : "roles/visionai.operatorEditor",
    "operatorViewer" : "roles/visionai.operatorViewer",
    "packetReceiver" : "roles/visionai.packetReceiver",
    "packetSender" : "roles/visionai.packetSender",
    "processorEditor" : "roles/visionai.processorEditor",
    "processorViewer" : "roles/visionai.processorViewer",
    "seriesEditor" : "roles/visionai.seriesEditor",
    "seriesViewer" : "roles/visionai.seriesViewer",
    "streamEditor" : "roles/visionai.streamEditor",
    "streamViewer" : "roles/visionai.streamViewer",
    "viewer" : "roles/visionai.viewer"
  }
}

output "visualinspection" {
  description = "Roles of service visualinspection"
  value       = {
    "editor" : "roles/visualinspection.editor",
    "serviceAgent" : "roles/visualinspection.serviceAgent",
    "usageMetricsReporter" : "roles/visualinspection.usageMetricsReporter",
    "viewer" : "roles/visualinspection.viewer"
  }
}

output "browser" {
  description = "Roles of service browser"
  value       = {
    "" : "roles/browser"
  }
}

output "proximitybeacon" {
  description = "Roles of service proximitybeacon"
  value       = {
    "attachmentEditor" : "roles/proximitybeacon.attachmentEditor",
    "attachmentPublisher" : "roles/proximitybeacon.attachmentPublisher",
    "attachmentViewer" : "roles/proximitybeacon.attachmentViewer",
    "beaconEditor" : "roles/proximitybeacon.beaconEditor"
  }
}

output "pubsub" {
  description = "Roles of service pubsub"
  value       = {
    "admin" : "roles/pubsub.admin",
    "editor" : "roles/pubsub.editor",
    "publisher" : "roles/pubsub.publisher",
    "serviceAgent" : "roles/pubsub.serviceAgent",
    "subscriber" : "roles/pubsub.subscriber",
    "viewer" : "roles/pubsub.viewer"
  }
}

output "pubsublite" {
  description = "Roles of service pubsublite"
  value       = {
    "admin" : "roles/pubsublite.admin",
    "editor" : "roles/pubsublite.editor",
    "publisher" : "roles/pubsublite.publisher",
    "subscriber" : "roles/pubsublite.subscriber",
    "viewer" : "roles/pubsublite.viewer"
  }
}

output "rma" {
  description = "Roles of service rma"
  value       = {
    "admin" : "roles/rma.admin",
    "runner" : "roles/rma.runner",
    "viewer" : "roles/rma.viewer"
  }
}

output "recaptchaenterprise" {
  description = "Roles of service recaptchaenterprise"
  value       = {
    "admin" : "roles/recaptchaenterprise.admin",
    "agent" : "roles/recaptchaenterprise.agent",
    "viewer" : "roles/recaptchaenterprise.viewer"
  }
}

output "automlrecommendations" {
  description = "Roles of service automlrecommendations"
  value       = {
    "admin" : "roles/automlrecommendations.admin",
    "adminViewer" : "roles/automlrecommendations.adminViewer",
    "editor" : "roles/automlrecommendations.editor",
    "serviceAgent" : "roles/automlrecommendations.serviceAgent",
    "viewer" : "roles/automlrecommendations.viewer"
  }
}

output "resourcemanager" {
  description = "Roles of service resourcemanager"
  value       = {
    "folderAdmin" : "roles/resourcemanager.folderAdmin",
    "folderCreator" : "roles/resourcemanager.folderCreator",
    "folderEditor" : "roles/resourcemanager.folderEditor",
    "folderIamAdmin" : "roles/resourcemanager.folderIamAdmin",
    "folderMover" : "roles/resourcemanager.folderMover",
    "folderViewer" : "roles/resourcemanager.folderViewer",
    "lienModifier" : "roles/resourcemanager.lienModifier",
    "organizationAdmin" : "roles/resourcemanager.organizationAdmin",
    "organizationViewer" : "roles/resourcemanager.organizationViewer",
    "projectCreator" : "roles/resourcemanager.projectCreator",
    "projectDeleter" : "roles/resourcemanager.projectDeleter",
    "projectIamAdmin" : "roles/resourcemanager.projectIamAdmin",
    "projectMover" : "roles/resourcemanager.projectMover",
    "tagAdmin" : "roles/resourcemanager.tagAdmin",
    "tagHoldAdmin" : "roles/resourcemanager.tagHoldAdmin",
    "tagUser" : "roles/resourcemanager.tagUser",
    "tagViewer" : "roles/resourcemanager.tagViewer"
  }
}

output "resourcesettings" {
  description = "Roles of service resourcesettings"
  value       = {
    "admin" : "roles/resourcesettings.admin",
    "viewer" : "roles/resourcesettings.viewer"
  }
}

output "riskmanager" {
  description = "Roles of service riskmanager"
  value       = {
    "admin" : "roles/riskmanager.admin",
    "editor" : "roles/riskmanager.editor",
    "reviewer" : "roles/riskmanager.reviewer",
    "serviceAgent" : "roles/riskmanager.serviceAgent",
    "viewer" : "roles/riskmanager.viewer"
  }
}

output "secretmanager" {
  description = "Roles of service secretmanager"
  value       = {
    "admin" : "roles/secretmanager.admin",
    "secretAccessor" : "roles/secretmanager.secretAccessor",
    "secretVersionAdder" : "roles/secretmanager.secretVersionAdder",
    "secretVersionManager" : "roles/secretmanager.secretVersionManager",
    "viewer" : "roles/secretmanager.viewer"
  }
}

output "securitycenter" {
  description = "Roles of service securitycenter"
  value       = {
    "admin" : "roles/securitycenter.admin",
    "adminEditor" : "roles/securitycenter.adminEditor",
    "adminViewer" : "roles/securitycenter.adminViewer",
    "assetSecurityMarksWriter" : "roles/securitycenter.assetSecurityMarksWriter",
    "assetsDiscoveryRunner" : "roles/securitycenter.assetsDiscoveryRunner",
    "assetsViewer" : "roles/securitycenter.assetsViewer",
    "automationServiceAgent" : "roles/securitycenter.automationServiceAgent",
    "bigQueryExportsEditor" : "roles/securitycenter.bigQueryExportsEditor",
    "bigQueryExportsViewer" : "roles/securitycenter.bigQueryExportsViewer",
    "controlServiceAgent" : "roles/securitycenter.controlServiceAgent",
    "externalSystemsEditor" : "roles/securitycenter.externalSystemsEditor",
    "findingSecurityMarksWriter" : "roles/securitycenter.findingSecurityMarksWriter",
    "findingsBulkMuteEditor" : "roles/securitycenter.findingsBulkMuteEditor",
    "findingsEditor" : "roles/securitycenter.findingsEditor",
    "findingsMuteSetter" : "roles/securitycenter.findingsMuteSetter",
    "findingsStateSetter" : "roles/securitycenter.findingsStateSetter",
    "findingsViewer" : "roles/securitycenter.findingsViewer",
    "findingsWorkflowStateSetter" : "roles/securitycenter.findingsWorkflowStateSetter",
    "integrationExecutorServiceAgent" : "roles/securitycenter.integrationExecutorServiceAgent",
    "muteConfigsEditor" : "roles/securitycenter.muteConfigsEditor",
    "muteConfigsViewer" : "roles/securitycenter.muteConfigsViewer",
    "notificationConfigEditor" : "roles/securitycenter.notificationConfigEditor",
    "notificationConfigViewer" : "roles/securitycenter.notificationConfigViewer",
    "notificationServiceAgent" : "roles/securitycenter.notificationServiceAgent",
    "securityHealthAnalyticsServiceAgent" : "roles/securitycenter.securityHealthAnalyticsServiceAgent",
    "securityResponseServiceAgent" : "roles/securitycenter.securityResponseServiceAgent",
    "serviceAgent" : "roles/securitycenter.serviceAgent",
    "settingsAdmin" : "roles/securitycenter.settingsAdmin",
    "settingsEditor" : "roles/securitycenter.settingsEditor",
    "settingsViewer" : "roles/securitycenter.settingsViewer",
    "sourcesAdmin" : "roles/securitycenter.sourcesAdmin",
    "sourcesEditor" : "roles/securitycenter.sourcesEditor",
    "sourcesViewer" : "roles/securitycenter.sourcesViewer"
  }
}

output "vpcaccess" {
  description = "Roles of service vpcaccess"
  value       = {
    "admin" : "roles/vpcaccess.admin",
    "serviceAgent" : "roles/vpcaccess.serviceAgent",
    "user" : "roles/vpcaccess.user",
    "viewer" : "roles/vpcaccess.viewer"
  }
}

output "aiplatform" {
  description = "Roles of service aiplatform"
  value       = {
    "admin" : "roles/aiplatform.admin",
    "customCodeServiceAgent" : "roles/aiplatform.customCodeServiceAgent",
    "entityTypeOwner" : "roles/aiplatform.entityTypeOwner",
    "featurestoreAdmin" : "roles/aiplatform.featurestoreAdmin",
    "featurestoreDataViewer" : "roles/aiplatform.featurestoreDataViewer",
    "featurestoreDataWriter" : "roles/aiplatform.featurestoreDataWriter",
    "featurestoreInstanceCreator" : "roles/aiplatform.featurestoreInstanceCreator",
    "featurestoreResourceViewer" : "roles/aiplatform.featurestoreResourceViewer",
    "featurestoreUser" : "roles/aiplatform.featurestoreUser",
    "migrator" : "roles/aiplatform.migrator",
    "serviceAgent" : "roles/aiplatform.serviceAgent",
    "tensorboardWebAppUser" : "roles/aiplatform.tensorboardWebAppUser",
    "user" : "roles/aiplatform.user",
    "viewer" : "roles/aiplatform.viewer"
  }
}

output "anthos" {
  description = "Roles of service anthos"
  value       = {
    "serviceAgent" : "roles/anthos.serviceAgent"
  }
}

output "anthosaudit" {
  description = "Roles of service anthosaudit"
  value       = {
    "serviceAgent" : "roles/anthosaudit.serviceAgent"
  }
}

output "anthosconfigmanagement" {
  description = "Roles of service anthosconfigmanagement"
  value       = {
    "serviceAgent" : "roles/anthosconfigmanagement.serviceAgent"
  }
}

output "anthosidentityservice" {
  description = "Roles of service anthosidentityservice"
  value       = {
    "serviceAgent" : "roles/anthosidentityservice.serviceAgent"
  }
}

output "anthosservicemesh" {
  description = "Roles of service anthosservicemesh"
  value       = {
    "serviceAgent" : "roles/anthosservicemesh.serviceAgent"
  }
}

output "anthossupport" {
  description = "Roles of service anthossupport"
  value       = {
    "serviceAgent" : "roles/anthossupport.serviceAgent"
  }
}

output "apigateway_management" {
  description = "Roles of service apigateway_management"
  value       = {
    "serviceAgent" : "roles/apigateway_management.serviceAgent"
  }
}

output "appdevelopmentexperience" {
  description = "Roles of service appdevelopmentexperience"
  value       = {
    "serviceAgent" : "roles/appdevelopmentexperience.serviceAgent"
  }
}

output "appengineflex" {
  description = "Roles of service appengineflex"
  value       = {
    "serviceAgent" : "roles/appengineflex.serviceAgent"
  }
}

output "bigqueryconnection" {
  description = "Roles of service bigqueryconnection"
  value       = {
    "serviceAgent" : "roles/bigqueryconnection.serviceAgent"
  }
}

output "bigquerydatatransfer" {
  description = "Roles of service bigquerydatatransfer"
  value       = {
    "serviceAgent" : "roles/bigquerydatatransfer.serviceAgent"
  }
}

output "cloudtpu" {
  description = "Roles of service cloudtpu"
  value       = {
    "serviceAgent" : "roles/cloudtpu.serviceAgent"
  }
}

output "compliancescanning" {
  description = "Roles of service compliancescanning"
  value       = {
    "ServiceAgent" : "roles/compliancescanning.ServiceAgent"
  }
}

output "containerregistry" {
  description = "Roles of service containerregistry"
  value       = {
    "ServiceAgent" : "roles/containerregistry.ServiceAgent"
  }
}

output "containerscanning" {
  description = "Roles of service containerscanning"
  value       = {
    "ServiceAgent" : "roles/containerscanning.ServiceAgent"
  }
}

output "containerthreatdetection" {
  description = "Roles of service containerthreatdetection"
  value       = {
    "serviceAgent" : "roles/containerthreatdetection.serviceAgent"
  }
}

output "documentaicore" {
  description = "Roles of service documentaicore"
  value       = {
    "serviceAgent" : "roles/documentaicore.serviceAgent"
  }
}

output "endpointsportal" {
  description = "Roles of service endpointsportal"
  value       = {
    "serviceAgent" : "roles/endpointsportal.serviceAgent"
  }
}

output "firebasemods" {
  description = "Roles of service firebasemods"
  value       = {
    "serviceAgent" : "roles/firebasemods.serviceAgent"
  }
}

output "firestore" {
  description = "Roles of service firestore"
  value       = {
    "serviceAgent" : "roles/firestore.serviceAgent"
  }
}

output "firewallinsights" {
  description = "Roles of service firewallinsights"
  value       = {
    "serviceAgent" : "roles/firewallinsights.serviceAgent"
  }
}

output "kuberun" {
  description = "Roles of service kuberun"
  value       = {
    "eventsControlPlaneServiceAgent" : "roles/kuberun.eventsControlPlaneServiceAgent",
    "eventsDataPlaneServiceAgent" : "roles/kuberun.eventsDataPlaneServiceAgent"
  }
}

output "mediaasset" {
  description = "Roles of service mediaasset"
  value       = {
    "serviceAgent" : "roles/mediaasset.serviceAgent"
  }
}

output "meshcontrolplane" {
  description = "Roles of service meshcontrolplane"
  value       = {
    "serviceAgent" : "roles/meshcontrolplane.serviceAgent"
  }
}

output "meshdataplane" {
  description = "Roles of service meshdataplane"
  value       = {
    "serviceAgent" : "roles/meshdataplane.serviceAgent"
  }
}

output "multiclusteringress" {
  description = "Roles of service multiclusteringress"
  value       = {
    "serviceAgent" : "roles/multiclusteringress.serviceAgent"
  }
}

output "multiclustermetering" {
  description = "Roles of service multiclustermetering"
  value       = {
    "serviceAgent" : "roles/multiclustermetering.serviceAgent"
  }
}

output "rapidmigrationassessment" {
  description = "Roles of service rapidmigrationassessment"
  value       = {
    "serviceAgent" : "roles/rapidmigrationassessment.serviceAgent"
  }
}

output "servicedirectory" {
  description = "Roles of service servicedirectory"
  value       = {
    "admin" : "roles/servicedirectory.admin",
    "editor" : "roles/servicedirectory.editor",
    "networkAttacher" : "roles/servicedirectory.networkAttacher",
    "pscAuthorizedService" : "roles/servicedirectory.pscAuthorizedService",
    "serviceAgent" : "roles/servicedirectory.serviceAgent",
    "viewer" : "roles/servicedirectory.viewer"
  }
}

output "servicenetworking" {
  description = "Roles of service servicenetworking"
  value       = {
    "networksAdmin" : "roles/servicenetworking.networksAdmin",
    "serviceAgent" : "roles/servicenetworking.serviceAgent"
  }
}

output "sourcerepo" {
  description = "Roles of service sourcerepo"
  value       = {
    "serviceAgent" : "roles/sourcerepo.serviceAgent"
  }
}

output "sqlx" {
  description = "Roles of service sqlx"
  value       = {
    "serviceAgent" : "roles/sqlx.serviceAgent"
  }
}

output "transcoder" {
  description = "Roles of service transcoder"
  value       = {
    "admin" : "roles/transcoder.admin",
    "serviceAgent" : "roles/transcoder.serviceAgent",
    "viewer" : "roles/transcoder.viewer"
  }
}

output "websecurityscanner" {
  description = "Roles of service websecurityscanner"
  value       = {
    "serviceAgent" : "roles/websecurityscanner.serviceAgent"
  }
}

output "workflows" {
  description = "Roles of service workflows"
  value       = {
    "admin" : "roles/workflows.admin",
    "editor" : "roles/workflows.editor",
    "invoker" : "roles/workflows.invoker",
    "serviceAgent" : "roles/workflows.serviceAgent",
    "viewer" : "roles/workflows.viewer"
  }
}

output "workloadcertificate" {
  description = "Roles of service workloadcertificate"
  value       = {
    "serviceAgent" : "roles/workloadcertificate.serviceAgent"
  }
}

output "workloadmanager" {
  description = "Roles of service workloadmanager"
  value       = {
    "admin" : "roles/workloadmanager.admin",
    "serviceAgent" : "roles/workloadmanager.serviceAgent",
    "viewer" : "roles/workloadmanager.viewer",
    "worker" : "roles/workloadmanager.worker"
  }
}

output "serviceconsumermanagement" {
  description = "Roles of service serviceconsumermanagement"
  value       = {
    "tenancyUnitsAdmin" : "roles/serviceconsumermanagement.tenancyUnitsAdmin",
    "tenancyUnitsViewer" : "roles/serviceconsumermanagement.tenancyUnitsViewer"
  }
}

output "serverless" {
  description = "Roles of service serverless"
  value       = {
    "serviceAgent" : "roles/serverless.serviceAgent"
  }
}

output "servicemanagement" {
  description = "Roles of service servicemanagement"
  value       = {
    "admin" : "roles/servicemanagement.admin",
    "configEditor" : "roles/servicemanagement.configEditor",
    "quotaAdmin" : "roles/servicemanagement.quotaAdmin",
    "quotaViewer" : "roles/servicemanagement.quotaViewer",
    "reporter" : "roles/servicemanagement.reporter",
    "serviceConsumer" : "roles/servicemanagement.serviceConsumer",
    "serviceController" : "roles/servicemanagement.serviceController"
  }
}

output "serviceusage" {
  description = "Roles of service serviceusage"
  value       = {
    "apiKeysAdmin" : "roles/serviceusage.apiKeysAdmin",
    "apiKeysViewer" : "roles/serviceusage.apiKeysViewer",
    "serviceUsageAdmin" : "roles/serviceusage.serviceUsageAdmin",
    "serviceUsageConsumer" : "roles/serviceusage.serviceUsageConsumer",
    "serviceUsageViewer" : "roles/serviceusage.serviceUsageViewer"
  }
}

output "source" {
  description = "Roles of service source"
  value       = {
    "admin" : "roles/source.admin",
    "reader" : "roles/source.reader",
    "writer" : "roles/source.writer"
  }
}

output "stackdriver" {
  description = "Roles of service stackdriver"
  value       = {
    "accounts_editor" : "roles/stackdriver.accounts.editor",
    "accounts_viewer" : "roles/stackdriver.accounts.viewer",
    "resourceMetadata_writer" : "roles/stackdriver.resourceMetadata.writer"
  }
}

output "stream" {
  description = "Roles of service stream"
  value       = {
    "admin" : "roles/stream.admin",
    "contentAdmin" : "roles/stream.contentAdmin",
    "contentBuilder" : "roles/stream.contentBuilder",
    "instanceAdmin" : "roles/stream.instanceAdmin",
    "viewer" : "roles/stream.viewer"
  }
}

output "cloudsupport" {
  description = "Roles of service cloudsupport"
  value       = {
    "admin" : "roles/cloudsupport.admin",
    "techSupportEditor" : "roles/cloudsupport.techSupportEditor",
    "techSupportViewer" : "roles/cloudsupport.techSupportViewer",
    "viewer" : "roles/cloudsupport.viewer"
  }
}

output "dellemccloudonefs" {
  description = "Roles of service dellemccloudonefs"
  value       = {
    "admin" : "roles/dellemccloudonefs.admin",
    "user" : "roles/dellemccloudonefs.user",
    "viewer" : "roles/dellemccloudonefs.viewer"
  }
}

output "netappcloudvolumes" {
  description = "Roles of service netappcloudvolumes"
  value       = {
    "admin" : "roles/netappcloudvolumes.admin",
    "viewer" : "roles/netappcloudvolumes.viewer"
  }
}

output "redisenterprisecloud" {
  description = "Roles of service redisenterprisecloud"
  value       = {
    "admin" : "roles/redisenterprisecloud.admin",
    "viewer" : "roles/redisenterprisecloud.viewer"
  }
}

output "transferappliance" {
  description = "Roles of service transferappliance"
  value       = {
    "admin" : "roles/transferappliance.admin",
    "viewer" : "roles/transferappliance.viewer"
  }
}

output "videostitcher" {
  description = "Roles of service videostitcher"
  value       = {
    "admin" : "roles/videostitcher.admin",
    "user" : "roles/videostitcher.user",
    "viewer" : "roles/videostitcher.viewer"
  }
}

output "vmwareengine" {
  description = "Roles of service vmwareengine"
  value       = {
    "vmwareengineAdmin" : "roles/vmwareengine.vmwareengineAdmin",
    "vmwareengineViewer" : "roles/vmwareengine.vmwareengineViewer"
  }
}
