#!/bin/sh

mkdir data

curl https://cloud.google.com/iam/docs/understanding-roles \
  | grep "roles/<wbr" \
  | sed "s#.*/>##g" \
  | sed "s#<.*##g" > data/gcp_roles.txt
