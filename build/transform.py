import json


def parse_roles(input_file):
    roles = {}

    lines = open(input_file, "r")
    for line in lines:
        clean_line = line.strip()
        tokens = clean_line.split('.')
        service = tokens[0]
        local_role = "_".join(tokens[1:])
        role_id = "roles/" + clean_line

        if service not in roles:
            roles[service] = {}

        roles[service][local_role] = role_id

    return roles


def print_roles(roles, output_file):
    file = open(output_file, "w")
    for service in roles:
        value = json.dumps(roles[service], sort_keys=True, indent=4)
        resource = """
output "%s" {
    description = "Roles of service %s"
    value       = %s
}
""" % (service, service, value)
        file.write(resource)

    file.close()


def main(input_file, output_file):
    roles = parse_roles(input_file)
    print_roles(roles, output_file)


if __name__ == '__main__':
    main("data/gcp_roles.txt", "../output.tf")
